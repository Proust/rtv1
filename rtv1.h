/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf3d.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apietush <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/22 15:19:13 by apietush          #+#    #+#             */
/*   Updated: 2017/12/05 22:05:21 by apietush         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RTV1_RTV1_H
# define RTV1_RTV1_H

# include "libft/libft.h"
# include "minilibx_macos/mlx.h"
# include <fcntl.h>
# include <math.h>
# include <libc.h>
# include <pthread.h>

# define W_H 600
# define W_W 900

typedef struct			s_vect
{
	double				x;
	double				y;
	double				z;
}						t_vect;

typedef struct			s_col
{
	unsigned char		r;
	unsigned char		g;
	unsigned char		b;
	unsigned char		a;
}						t_col;

typedef struct			s_bid_list
{
	void				*data;
	struct s_bid_list	*prew;
	struct s_bid_list	*next;
}						t_bid_list;

typedef struct			s_light
{
	struct s_vect		pos;
	double				intense;
}						t_light;

typedef struct			s_camera
{
	struct s_vect		position;
	struct s_vect		alpha;
	struct s_vect		sin_al;
	struct s_vect		cos_al;
	double				distanse;
}						t_camera;

typedef struct			s_img
{
	void				*img_ptr;
	char				*img_str;
	int					bpp;
	int					sl;
	int					en;
}						t_img;

typedef struct			s_scene
{
	struct s_img		img;
	struct s_camera		cam;
	struct s_bid_list	*objs;
	struct s_bid_list	*lights;
	char				*name;
}						t_scene;

typedef struct			s_sphere
{
	struct s_vect		position;
	double				radius;
	struct s_col		color;
}						t_sphere;

typedef struct			s_cylinder
{
	struct s_vect		position;
	struct s_vect		direction;
	double				radius;
	struct s_col		color;
}						t_cylinder;

typedef struct			s_parab
{
	struct s_vect		position;
	struct s_vect		direction;
	double				k;
	struct s_col		color;
}						t_parab;

typedef struct			s_cone
{
	struct s_vect		position;
	struct s_vect		direction;
	double				angel;
	struct s_col		color;
}						t_cone;

typedef struct			s_plane
{
	struct s_vect		position;
	struct s_vect		direction;
	struct s_col		color;
}						t_plane;

typedef struct			s_obj
{
	void				*data;
	unsigned char		type;
	int					(*intersect)(const void *data,
									const t_vect *start_pnt,
									const t_vect *vect,
									t_vect *inters_pnt);
	struct s_col		*(*get_col)(const void *data,
									const t_vect *inters_pnt);
	struct s_vect		(*get_norm_vect)(const void *data,
										const t_vect *inters_pnt);
	void				(*destruct_data)(void *data);
}						t_obj;

typedef struct			s_env
{
	void				*mlx;
	void				*win;
	char				*w_name;
	struct s_bid_list	*scenes;
}						t_env;

t_env					*ft_new_s_env(void *mlx, void *win);
void					ft_destroy_env(t_env *env);
void					err_mess(char *mess, t_env *env);
void					reinit_cond(t_env *env);
int						ft_raycast(t_env *env);
void					ft_hooks(t_env *env);
int						check_d(double *t, t_vect *a_b_c);
t_bid_list				*add_bid_lst(t_bid_list *lst, void *data);
t_light					*new_light(t_vect pos, double intens);
int						find_lights(const t_vect *dirr, const t_obj *obj,
								t_vect *inters_pnt, const t_scene *scene);
void					render(const t_env *env, const t_scene *scene);
int						ft_rgb_to_int(int r, int g, int b);

/*
** vector
*/

t_vect					new_v(double x, double y, double z);
double					vect_multipl(t_vect a, t_vect b);
double					vect_mod_sqr(t_vect v);
double					vect_len(t_vect v);
t_vect					vect_substr(t_vect start, t_vect end);
void					vect_normalise(t_vect *v);
t_vect					vect_multipl_const(t_vect a, double c);

t_col					new_col(unsigned char r, unsigned char g,
								unsigned char b, unsigned char a);

/*
** sphere
*/

t_obj					*new_sphere(t_vect pos, double r, t_col col);
int						intersect_sphere(const void *data,
										const t_vect *start_pnt,
										const t_vect *vect,
										t_vect *inters_pnt);
t_col					*get_col_sphere(const void *data,
										const t_vect *inters_pnt);
void					destruct_sphere(void *data);
t_vect					get_norm_vect_sphere(const void *data,
												const t_vect *inters_pnt);

/*
** cone
*/

t_obj					*new_cone(t_vect pos, t_vect dir, double angel,
									t_col col);
int						intersect_cone(const void *data,
											const t_vect *start_pnt,
											const t_vect *vect,
											t_vect *inters_pnt);
t_col					*get_col_cone(const void *data,
										const t_vect *inters_pnt);
void					destruct_cone(void *data);
t_vect					get_norm_vect_cone(const void *data,
											const t_vect *inters_pnt);

/*
** cylinder
*/

t_obj					*new_cylinder(t_vect pos, t_vect dir, double r,
										t_col col);
int						intersect_cylinder(const void *data,
											const t_vect *start_pnt,
											const t_vect *vect,
											t_vect *inters_pnt);
t_col					*get_col_cylinder(const void *data,
											const t_vect *inters_pnt);
void					destruct_cylinder(void *data);
t_vect					get_norm_vect_cylinder(const void *data,
										const t_vect *inters_pnt);

/*
** plane
*/

t_obj					*new_plane(t_vect pos, t_vect dir, t_col col);
int						intersect_plane(const void *data,
										const t_vect *start_pnt,
										const t_vect *vect,
										t_vect *inters_pnt);
t_col					*get_col_plane(const void *data,
										const t_vect *inters_pnt);
void					destruct_plane(void *data);
t_vect					get_norm_vect_plane(const void *data,
											const t_vect *inters_pnt);


/*
** cam
*/

void					init_camera(t_camera *cam);
t_vect					init_cam_ray(int x, int y, const t_camera *cam);

/*
** scene
*/

t_scene					*scene_init(int n, t_env *env);

#endif
