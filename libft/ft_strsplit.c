/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apietush <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/09 21:51:10 by apietush          #+#    #+#             */
/*   Updated: 2016/12/11 14:28:14 by apietush         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static char	**countstrings(char const *s, char c)
{
	size_t	counter;
	size_t	strings;
	char	**bigarray;

	counter = 0;
	strings = 0;
	while (s[counter])
	{
		if (s[counter] != c && (s[counter + 1] == c || s[counter + 1] == '\0'))
			strings++;
		counter++;
	}
	if ((bigarray = (char**)malloc(sizeof(char *) * (strings + 1))) == NULL)
		return (NULL);
	return (bigarray);
}

char		**ft_strsplit(char const *s, char c)
{
	size_t	i;
	size_t	len;
	size_t	j;
	char	**str;

	i = 0;
	len = 0;
	j = 0;
	if (!s || !(str = countstrings(s, c)))
		return (NULL);
	while (s[i] != '\0')
	{
		if (s[i] != c)
		{
			len++;
			if (s[i + 1] == c || s[i + 1] == '\0')
			{
				str[j++] = ft_strsub(s, i - len + 1, len);
				len = 0;
			}
		}
		i++;
	}
	str[j] = NULL;
	return (str);
}
