/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apietush <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/27 18:06:54 by apietush          #+#    #+#             */
/*   Updated: 2016/12/15 13:08:45 by apietush         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_atoi(const char *s)
{
	size_t					i;
	unsigned long long int	number;
	int						sign;

	i = 0;
	number = 0;
	sign = 0;
	while ((s[i] > 8 && s[i] < 14) || s[i] == 32)
		i++;
	if (s[i] == '-')
		sign = (s[i++] == '-') ? 1 : 0;
	if (sign != 1 && s[i] == '+')
		i++;
	while (s[i] >= '0' && s[i] <= '9')
	{
		number = number * 10 + (s[i++] - '0');
		if (number > 9223372036854775807 && sign == 0)
			return (-1);
		else if (number > 9223372036854775807 && sign == -1)
			return (0);
	}
	if (sign == 1)
		return (-number);
	return (number);
}
