/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apietush <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/30 03:31:19 by apietush          #+#    #+#             */
/*   Updated: 2017/10/31 03:18:53 by apietush         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include "rtv1.h"

int					ft_rgb_to_int(int r, int g, int b)
{
	return (b + g * 256 + r * 65536);
}

t_col				new_col(unsigned char r, unsigned char g,
							unsigned char b, unsigned char a)
{
	t_col			res;

	res.r = r;
	res.g = g;
	res.b = b;
	res.a = a;
	return (res);
}
