/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   plane.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apietush <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/30 03:24:55 by apietush          #+#    #+#             */
/*   Updated: 2017/10/30 03:24:58 by apietush         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

int					intersect_plane(const void *data,
									const t_vect *start_pnt,
									const t_vect *vect,
									t_vect *inters_pnt)
{
	const t_plane	*plane;
	double			d;
	double			t;
	t_vect			dist;

	plane = (t_plane *)data;
	d = vect_multipl(*vect, plane->direction);
	if (d == 0.0)
		return (0);
	dist = vect_substr(plane->position, *start_pnt);
	t = vect_multipl(dist, plane->direction) / d;
	if (t < 0.000001 || t > 1.0e30)
		return (0);
	dist = vect_multipl_const(*vect, t);
	inters_pnt->x = start_pnt->x + dist.x;
	inters_pnt->y = start_pnt->y + dist.y;
	inters_pnt->z = start_pnt->z + dist.z;
	return (1);
}

t_col				*get_col_plane(const void *data,
							const t_vect *inters_pnt)
{
	const t_plane	*plane;
	t_col			*col;

	col = malloc(sizeof(*col));
	plane = (t_plane *)data;
	col->r = plane->color.r;
	col->g = plane->color.g;
	col->b = plane->color.b;
	col->a = plane->color.a;
	return (col);
}

void				destruct_plane(void *data)
{
	t_plane		*plane;

	plane = (t_plane *)data;
	free(plane);
}

t_vect				get_norm_vect_plane(const void *data,
										const t_vect *inters_pnt)
{
	t_plane			*plane;
	t_vect			res;

	plane = (t_plane *)data;
	res.x = plane->direction.x;
	res.y = plane->direction.y;
	res.z = plane->direction.z;
	vect_normalise(&res);
	return (res);
}

t_obj				*new_plane(t_vect pos, t_vect dir, t_col col)
{
	t_obj			*obj;
	t_plane			*plane;

	plane = malloc(sizeof(*plane));
	plane->position = pos;
	plane->color = col;
	plane->direction = dir;
	vect_normalise(&plane->direction);
	obj = malloc(sizeof(*obj));
	obj->data = (void *)plane;
	obj->type = 0;
	obj->intersect = intersect_plane;
	obj->get_col = get_col_plane;
	obj->destruct_data = destruct_plane;
	obj->get_norm_vect = get_norm_vect_plane;
	return (obj);
}
