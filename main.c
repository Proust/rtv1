/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apietush <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/30 03:25:33 by apietush          #+#    #+#             */
/*   Updated: 2017/10/30 03:25:34 by apietush         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

int				main(void)
{
	t_env		*env;
	t_scene		*scene;

	env = ft_new_s_env(NULL, NULL);
	if ((env->mlx = mlx_init()))
	{
		env->w_name = "RTv1";
		env->win = mlx_new_window(env->mlx, W_W, W_H, env->w_name);
		env->scenes = malloc(sizeof(t_bid_list));
		env->scenes->data = (void *)scene_init(3, env);
		scene = (t_scene *)env->scenes->data;
		env->w_name = scene->name;
		render(env, scene);
		mlx_put_image_to_window(env->mlx, env->win, scene->img.img_ptr, 0, 0);
		ft_hooks(env);
		mlx_loop(env->mlx);
	}
	return (0);
}
