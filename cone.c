/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cone.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apietush <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/30 03:31:19 by apietush          #+#    #+#             */
/*   Updated: 2017/10/31 03:18:53 by apietush         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include "rtv1.h"

int					intersect_cone(const void *data,
									const t_vect *start_pnt,
									const t_vect *vect,
									t_vect *inters_pnt)
{
	const t_cone	*cone;
	double			t;
	t_vect			dist;
	t_vect			a_b_c;

	cone = (t_cone *)data;
	dist = vect_substr(*start_pnt, cone->position);
	a_b_c.x = vect_multipl(*vect, *vect) - (1 + pow(tan(cone->angel), 2))
				* pow(vect_multipl(*vect, cone->direction), 2);
	a_b_c.y = 2 * (vect_multipl(*vect, dist) - (1 + pow(tan(cone->angel), 2))
				* vect_multipl(*vect, cone->direction)
				* vect_multipl(dist, cone->direction));
	a_b_c.z = vect_multipl(dist, dist) - (1 + pow(tan(cone->angel), 2))
				* pow(vect_multipl(dist, cone->direction), 2);
	if (!check_d(&t, &a_b_c))
		return (0);
	dist = vect_multipl_const(*vect, t);
	inters_pnt->x = start_pnt->x + dist.x;
	inters_pnt->y = start_pnt->y + dist.y;
	inters_pnt->z = start_pnt->z + dist.z;
	return (1);
}

t_col				*get_col_cone(const void *data,
							const t_vect *inters_pnt)
{
	const t_cone	*cone;
	t_col			*col;

	col = malloc(sizeof(*col));
	cone = (t_cone *)data;
	col->r = cone->color.r;
	col->g = cone->color.g;
	col->b = cone->color.b;
	col->a = cone->color.a;
	return (col);
}

void				destruct_cone(void *data)
{
	t_cone			*cone;

	cone = (t_cone *)data;
	free(cone);
}

t_vect				get_norm_vect_cone(const void *data,
											const t_vect *inters_pnt)
{
	t_cone			*cone;
	t_vect			vect;
	t_vect			vect_proj;
	t_vect			res;

	cone = (t_cone *)data;
	vect = vect_substr(*inters_pnt, cone->position);
	vect_proj = vect_multipl_const(cone->direction,
									vect_multipl(vect, cone->direction)
									/ vect_multipl(cone->direction,
													cone->direction));
	res = vect_multipl_const(vect_substr(vect, vect_proj),
								1 + pow(tan(cone->angel), 2));
	vect_normalise(&res);
	return (res);
}

t_obj				*new_cone(t_vect pos, t_vect dir, double angel, t_col col)
{
	t_obj			*obj;
	t_cone			*cone;

	cone = malloc(sizeof(*cone));
	cone->position = pos;
	cone->angel = angel;
	cone->color = col;
	cone->direction = dir;
	vect_normalise(&cone->direction);
	obj = malloc(sizeof(*obj));
	obj->type = 3;
	obj->data = (void *)cone;
	obj->intersect = intersect_cone;
	obj->get_col = get_col_cone;
	obj->destruct_data = destruct_cone;
	obj->get_norm_vect = get_norm_vect_cone;
	return (obj);
}
