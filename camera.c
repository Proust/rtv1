/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   camera.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apietush <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/30 03:31:19 by apietush          #+#    #+#             */
/*   Updated: 2017/10/31 03:18:53 by apietush         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

void		init_camera(t_camera *cam)
{
	cam->position.x = 0;
	cam->position.y = 0;
	cam->position.z = -1000;
	cam->distanse = 10;
	cam->alpha.x = 0;
	cam->alpha.y = 0;
	cam->alpha.z = 0;
	cam->sin_al.x = sin(cam->alpha.x);
	cam->sin_al.y = sin(cam->alpha.y);
	cam->sin_al.z = sin(cam->alpha.z);
	cam->cos_al.x = cos(cam->alpha.x);
	cam->cos_al.y = cos(cam->alpha.y);
	cam->cos_al.z = cos(cam->alpha.z);
}

t_vect		init_cam_ray(int x, int y, const t_camera *cam)
{
	t_vect	vect;

	vect = vect_substr(new_v(x + 0.5 - W_W / 2, y + 0.5 - W_H / 2,
					cam->distanse), cam->position);
	vect_normalise(&vect);
	return (vect);
}
