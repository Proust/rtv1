/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   helper.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apietush <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/30 22:57:03 by apietush          #+#    #+#             */
/*   Updated: 2017/10/30 22:57:06 by apietush         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

int					check_d(double *t, t_vect *a_b_c)
{
	double			d;
	double			sqrt_d;
	double			t1;
	double			t2;

	d = a_b_c->y * a_b_c->y - 4 * a_b_c->x * a_b_c->z;
	if (d < 0)
		return (0);
	sqrt_d = sqrt(d);
	t1 = (-a_b_c->y + sqrt_d) / (2 * a_b_c->x);
	t2 = (-a_b_c->y - sqrt_d) / (2 * a_b_c->x);
	if (t1 < 0 && t2 < 0)
		return (0);
	else if (t1 < t2)
		*t = t1 < 0.000001 ? t2 : t1;
	else
		*t = t2 < 0.000001 ? t1 : t2;
	if (*t < 0.00001)
		return (0);
	return (1);
}

void				vect_normalise(t_vect *v)
{
	double			mod;

	mod = vect_len(*v);
	v->x /= mod;
	v->y /= mod;
	v->z /= mod;
}
