/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hooks.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apietush <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/31 03:19:59 by apietush          #+#    #+#             */
/*   Updated: 2017/10/31 03:20:00 by apietush         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

static int		ft_key_press(int kc, t_env *env)
{
	if (kc == 53)
		ft_destroy_env(env);
	return (0);
}

static int		ft_expose(t_env *env)
{
	return (0);
}

static int		ft_close_x(t_env *env)
{
	ft_destroy_env(env);
	return (0);
}

void			ft_hooks(t_env *env)
{
	mlx_expose_hook(env->win, ft_expose, env);
	mlx_hook(env->win, 2, 1L << 0, ft_key_press, env);
	mlx_hook(env->win, 17, 1L << 17, ft_close_x, env);
}
