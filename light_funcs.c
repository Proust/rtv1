/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   light_funcs.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apietush <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/30 03:57:33 by apietush          #+#    #+#             */
/*   Updated: 2017/10/30 03:57:35 by apietush         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

t_light			*new_light(t_vect pos, double intens)
{
	t_light		*res;

	res = malloc(sizeof(*res));
	res->pos = pos;
	res->intense = intens;
	return (res);
}
