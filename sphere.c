/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sphere.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apietush <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/24 23:43:45 by apietush          #+#    #+#             */
/*   Updated: 2017/10/24 23:43:47 by apietush         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

int					intersect_sphere(const void *data,
									const t_vect *start_pnt,
									const t_vect *vect,
									t_vect *inters_pnt)
{
	const t_sphere	*sphere;
	double			t;
	t_vect			dist;
	t_vect			a_b_c;

	sphere = (t_sphere *)data;
	a_b_c.x = vect_multipl(*vect, *vect);
	dist = vect_substr(*start_pnt, sphere->position);
	a_b_c.y = 2 * vect_multipl(*vect, dist);
	a_b_c.z = vect_multipl(dist, dist)
			- sphere->radius * sphere->radius;
	if (!check_d(&t, &a_b_c))
		return (0);
	dist = vect_multipl_const(*vect, t);
	inters_pnt->x = start_pnt->x + dist.x;
	inters_pnt->y = start_pnt->y + dist.y;
	inters_pnt->z = start_pnt->z + dist.z;
	return (1);
}

t_col				*get_col_sphere(const void *data,
							const t_vect *inters_pnt)
{
	const t_sphere	*sphere;
	t_col			*col;

	col = malloc(sizeof(*col));
	sphere = (t_sphere *)data;
	col->r = sphere->color.r;
	col->g = sphere->color.g;
	col->b = sphere->color.b;
	col->a = sphere->color.a;
	return (col);
}

void				destruct_sphere(void *data)
{
	t_sphere		*sphere;

	sphere = (t_sphere *)data;
	free(sphere);
}

t_vect				get_norm_vect_sphere(const void *data,
											const t_vect *inters_pnt)
{
	t_sphere		*sphere;
	t_vect			vect;

	sphere = (t_sphere *)data;
	vect = vect_substr(*inters_pnt, sphere->position);
	vect_normalise(&vect);
	return (vect);
}

t_obj				*new_sphere(t_vect pos, double r, t_col col)
{
	t_obj			*obj;
	t_sphere		*sphere;

	sphere = malloc(sizeof(*sphere));
	sphere->position = pos;
	sphere->radius = r;
	sphere->color = col;
	obj = malloc(sizeof(*obj));
	obj->data = (void *)sphere;
	obj->type = 1;
	obj->intersect = intersect_sphere;
	obj->get_col = get_col_sphere;
	obj->destruct_data = destruct_sphere;
	obj->get_norm_vect = get_norm_vect_sphere;
	return (obj);
}
