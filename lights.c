/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lights.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apietush <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/31 03:15:43 by apietush          #+#    #+#             */
/*   Updated: 2017/10/31 03:15:44 by apietush         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

int					get_int_col(const t_obj *obj, const t_vect *inters_pnt,
								double *k)
{
	t_col			*col;
	int				int_col;

	col = obj->get_col(obj->data, inters_pnt);
	col->r = (unsigned char)fmin(fmax(col->r *
									(k[0] + 0.3) + k[1] * 255, 0), 255);
	col->g = (unsigned char)fmin(fmax(col->g *
									(k[0] + 0.3) + k[1] * 255, 0), 255);
	col->b = (unsigned char)fmin(fmax(col->b *
									(k[0] + 0.3) + k[1] * 255, 0), 255);
	int_col = ft_rgb_to_int(col->r, col->g, col->b);
	free(col);
	free(k);
	return (int_col);
}

char				find_shadow(const t_vect *inters_pnt, const t_scene *scene,
								const t_obj *obj, t_vect light_ray)
{
	char			flag;
	t_obj			*obj_shed;
	t_bid_list		*objs;
	t_vect			inters_pnt_shed;
	double			light_ray_len;

	objs = scene->objs;
	flag = 0;
	light_ray_len = vect_len(light_ray);
	vect_normalise(&light_ray);
	while (objs)
	{
		obj_shed = (t_obj *)objs->data;
		if ((obj_shed->intersect(obj_shed->data, inters_pnt, &light_ray,
								&inters_pnt_shed)))
		{
			if (light_ray_len >= vect_len(vect_substr(*inters_pnt,
				inters_pnt_shed)) && (obj_shed != obj || (obj_shed == obj
				&& !scene->objs->next && !scene->objs->prew)))
				flag = 1;
		}
		objs = objs->next;
	}
	return (flag);
}

t_vect				get_reflect_ray(const t_vect *dirr, t_vect *normal)
{
	t_vect			reflect;

	reflect = vect_substr(*dirr, vect_multipl_const((*normal),
						2.0 * vect_multipl(*dirr, (*normal))));
	vect_normalise(&reflect);
	return (reflect);
}

int					find_lights(const t_vect *dirr, const t_obj *obj,
								t_vect *inters_pnt, const t_scene *scene)
{
	t_bid_list		*lights;
	double			*k;
	t_vect			light_ray;
	t_vect			normal;
	t_vect			reflect_ray;

	k = ft_memalloc(sizeof(double) * 2);
	lights = scene->lights;
	while (lights)
	{
		light_ray = vect_substr(((t_light *)lights->data)->pos, *inters_pnt);
		if (!find_shadow(inters_pnt, scene, obj, light_ray))
		{
			vect_normalise(&light_ray);
			normal = obj->get_norm_vect(obj->data, inters_pnt);
			if (!obj->type && vect_multipl(normal, light_ray) < 0)
				normal = vect_multipl_const(normal, -1);
			reflect_ray = get_reflect_ray(dirr, &normal);
			k[0] += fmax(vect_multipl(light_ray, normal), 0);
			if (vect_multipl(reflect_ray, light_ray) > 0)
				k[1] += fmax(pow(vect_multipl(reflect_ray, light_ray), 10), 0);
		}
		lights = lights->next;
	}
	return (get_int_col(obj, inters_pnt, k));
}
