/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   s_env_functions.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apietush <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/22 13:53:59 by apietush          #+#    #+#             */
/*   Updated: 2017/10/22 13:54:01 by apietush         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

t_env			*ft_new_s_env(void *mlx, void *win)
{
	t_env		*env;

	env = (t_env *)malloc(sizeof(struct s_env));
	env->mlx = mlx;
	env->win = win;
	env->w_name = NULL;
	env->scenes = NULL;
	return (env);
}

void			ft_destroy_env(t_env *env)
{
	if (env->win)
		mlx_destroy_window(env->mlx, env->win);
	free(env->scenes);
	free(env);
	exit(0);
}
