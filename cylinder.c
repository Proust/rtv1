/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cylinder.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apietush <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/30 03:31:19 by apietush          #+#    #+#             */
/*   Updated: 2017/10/31 03:18:53 by apietush         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include "rtv1.h"

int					intersect_cylinder(const void *data,
										const t_vect *start_pnt,
										const t_vect *vect,
										t_vect *inters_pnt)
{
	t_cylinder		*cylinder;
	double			t;
	t_vect			dist;
	t_vect			a_b_c;

	cylinder = (t_cylinder *)data;
	dist = vect_substr(*start_pnt, cylinder->position);
	a_b_c.x = vect_multipl(*vect, *vect)
				- pow(vect_multipl(*vect, cylinder->direction), 2);
	a_b_c.y = 2 * (vect_multipl(*vect, dist)
					- vect_multipl(*vect, cylinder->direction)
					* vect_multipl(dist, cylinder->direction));
	a_b_c.z = vect_multipl(dist, dist)
				- pow((cylinder->radius), 2)
				- pow(vect_multipl(dist, cylinder->direction), 2);
	if (!check_d(&t, &a_b_c))
		return (0);
	dist = vect_multipl_const(*vect, t);
	inters_pnt->x = start_pnt->x + dist.x;
	inters_pnt->y = start_pnt->y + dist.y;
	inters_pnt->z = start_pnt->z + dist.z;
	return (1);
}

t_col				*get_col_cylinder(const void *data,
										const t_vect *inters_pnt)
{
	t_cylinder		*cylinder;
	t_col			*col;

	col = malloc(sizeof(*col));
	cylinder = (t_cylinder *)data;
	col->r = cylinder->color.r;
	col->g = cylinder->color.g;
	col->b = cylinder->color.b;
	col->a = cylinder->color.a;
	return (col);
}

void				destruct_cylinder(void *data)
{
	t_cylinder		*cylinder;

	cylinder = (t_cylinder *)data;
	free(cylinder);
}

t_vect				get_norm_vect_cylinder(const void *data,
											const t_vect *inters_pnt)
{
	t_cylinder		*cylinder;
	t_vect			vect;
	t_vect			vect_proj;
	t_vect			res;

	cylinder = (t_cylinder *)data;
	vect = vect_substr(*inters_pnt, cylinder->position);
	vect_proj = vect_multipl_const(cylinder->direction,
								vect_multipl(vect, cylinder->direction));
	res = vect_substr(vect, vect_proj);
	vect_normalise(&res);
	return (res);
}

t_obj				*new_cylinder(t_vect pos, t_vect dir, double r, t_col col)
{
	t_obj			*obj;
	t_cylinder		*cylinder;

	cylinder = malloc(sizeof(*cylinder));
	cylinder->position = pos;
	cylinder->radius = r;
	cylinder->color = col;
	cylinder->direction = dir;
	vect_normalise(&cylinder->direction);
	obj = malloc(sizeof(*obj));
	obj->type = 2;
	obj->data = (void *)cylinder;
	obj->intersect = intersect_cylinder;
	obj->get_col = get_col_cylinder;
	obj->destruct_data = destruct_cylinder;
	obj->get_norm_vect = get_norm_vect_cylinder;
	return (obj);
}
