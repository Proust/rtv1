/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   scene.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apietush <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/30 03:28:17 by apietush          #+#    #+#             */
/*   Updated: 2017/10/30 03:28:18 by apietush         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

t_scene			*scene_0(t_scene *scene)
{
	scene->lights = add_bid_lst(scene->lights,
								new_light(new_v(450, -300, -200), 1000));
	scene->lights = add_bid_lst(scene->lights,
								new_light(new_v(-450, -300, -100), 1000));
	scene->objs = add_bid_lst(scene->objs,
								new_sphere(new_v(-140, -50, 60), 49,
											new_col(170, 25, 160, 0)));
	scene->name = "Scene 0";
	return (scene);
}

t_scene			*scene_1(t_scene *scene)
{
	t_obj		*cone;

	scene = scene_0(scene);
	cone = new_cone(new_v(100, 0, 100), new_v(0.5, -1, 0.5), 0.3,
					new_col(140, 73, 150, 0));
	scene->objs = add_bid_lst(scene->objs, cone);
	scene->name = "Scene 1";
	return (scene);
}

t_scene			*scene_2(t_scene *scene)
{
	t_obj		*cylinder;

	scene = scene_1(scene);
	cylinder = new_cylinder(new_v(0, 30, 200), new_v(1, 1, -1), 40,
							new_col(86, 75, 150, 0));
	scene->objs = add_bid_lst(scene->objs, cylinder);
	scene->name = "Scene 1";
	return (scene);
}

t_scene			*scene_3(t_scene *scene)
{
	t_obj		*plane;

	scene = scene_2(scene);
	plane = new_plane(new_v(0, 50, 600), new_v(0, 0, 1), new_col(15, 97, 180, 0));
	scene->objs = add_bid_lst(scene->objs, plane);
	scene->name = "Scene 3";
	return (scene);
}

t_scene			*scene_init(int n, t_env *env)
{
	t_scene		*scene;

	scene = malloc(sizeof(*scene));
	scene->img.img_ptr = mlx_new_image(env->mlx, W_W, W_H);
	scene->img.img_str = mlx_get_data_addr(scene->img.img_ptr,
							&scene->img.bpp, &scene->img.sl, &scene->img.en);
	init_camera(&scene->cam);
	scene->objs = NULL;
	scene->lights = NULL;
	if (n == 0)
		return (scene_0(scene));
	if (n == 1)
		return (scene_1(scene));
	if (n == 2)
		return (scene_2(scene));
	if (n == 3)
		return (scene_3(scene));
	if (n == 4)
	{
		scene = scene_3(scene_2(scene_1(scene_0(scene))));
		scene->name = "Scene 4";
		return (scene);
	}
	return (NULL);
}
