/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   raytraycing.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apietush <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/30 03:28:08 by apietush          #+#    #+#             */
/*   Updated: 2017/10/30 03:28:10 by apietush         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

static void		find_closest_obj(const t_scene *scene,
								t_vect *dirr, t_obj **obj, t_vect *inters_pnt)
{
	t_bid_list	*objs;
	t_obj		*obj_cur;
	t_vect		inters_pnt_cur;
	double		min_t;
	double		cur_t;

	objs = scene->objs;
	*obj = NULL;
	min_t = 1.0e30;
	while (objs)
	{
		obj_cur = (t_obj *)objs->data;
		if (obj_cur->intersect(obj_cur->data, &scene->cam.position,
								dirr, &inters_pnt_cur))
		{
			cur_t = vect_len(vect_substr(inters_pnt_cur, scene->cam.position));
			if (cur_t < min_t)
			{
				*obj = obj_cur;
				min_t = cur_t;
				*inters_pnt = inters_pnt_cur;
			}
		}
		objs = objs->next;
	}
}

void			render(const t_env *env, const t_scene *scene)
{
	int			x;
	int			y;
	t_vect		dir;
	t_obj		*obj;
	t_vect		inters_pnt;

	y = 0;
	while (y < W_H)
	{
		x = 0;
		while (x < W_W)
		{
			dir = init_cam_ray(x, y, &scene->cam);
			find_closest_obj(scene, &dir, &obj, &inters_pnt);
			if (obj)
				*(unsigned int *)(scene->img.img_str + x * scene->img.bpp
						/ 8 + y * scene->img.sl) =
						mlx_get_color_value(env->mlx,
						find_lights(&dir, obj, &inters_pnt, scene));
						
			x++;
		}
		y++;
	}
}
