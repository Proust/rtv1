# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: apietush <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/12/21 20:30:50 by apietush          #+#    #+#              #
#    Updated: 2017/12/05 22:09:19 by apietush         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = RTv1

INCLUDES = -I./libft/libft.h\
		   -I./minilibx_macos/mlx.h\
		   -I./rtv1.h

LIBS = -L./libft -lft\
	   -L./minilibx_macos -lmlx

MLX = -lmlx -framework OpenGL -framework AppKit

SOURCES = main.c\
		  sphere.c\
		  cylinder.c\
		  cone.c\
		  plane.c\
		  vector.c\
		  helper.c\
		  camera.c\
		  s_env_functions.c\
		  scene.c\
		  raytraycing.c\
		  bid_list_funcs.c\
		  light_funcs.c\
		  color.c\
		  hooks.c\
		  lights.c\

OBJECTS = $(SOURCES:.c=.o)

FLAG = -c -Wall -Werror -Wextra -lpthread

all: libs $(NAME)

$(NAME): $(OBJECTS)
	@gcc -o $(NAME) $(OBJECTS) $(LIBS) $(MLX)

%.o: %.c $(INCLUDES) $(MLX)
	@gcc $(FLAG) -o $@ $<
	
clean:
	@rm -f $(OBJECTS)
	@$(MAKE) clean -C libft	
	@$(MAKE) clean -C minilibx_macos

fclean: clean
	@rm -f $(NAME)
	@$(MAKE) fclean -C libft

re: clean all

libs: 
	@$(MAKE)  -C libft
	@$(MAKE)  -C minilibx_macos
